#include "engine.h"
/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

void get_password(char *password)
{
	static struct termios oldt, newt;
	int i = 0;
	int c;

	while((c = getchar()) != '\n' && c != EOF);
    	/*saving the old settings of STDIN_FILENO and copy settings for resetting*/
    	tcgetattr( STDIN_FILENO, &oldt);
    	newt = oldt;
	
	/*setting the approriate bit in the termios struct*/
    	newt.c_lflag &= ~(ECHO);          

    	/*setting the new bits*/
    	tcsetattr( STDIN_FILENO, TCSANOW, &newt);

    	/*reading the password from the console*/
    	while ((c = getchar())!= '\n' && c != EOF && i < sizeof(password)){
        	password[i++] = c;
    	}
    	password[i] = '\0';

    	/*resetting our old STDIN_FILENO*/ 
    	tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
	printf("\n");
}


void clear_buffer(char * buffer)
{
	memset(buffer, 0, MAXDATASIZE);
}


bool receive_message(int socket_id, char * buffer)
{
	int numbytes;
	clear_buffer(buffer);
	if ((numbytes= recv(socket_id, buffer, MAXDATASIZE, 0)) == -1){
		perror("recv");
		return false;
	}
	buffer[numbytes] = '\0';

	return true;
}



bool send_message(int socket_id, char * message)
{
	if (send(socket_id, message, strlen(message),0) == -1){
		perror("send");
		return false;
	}

	return true;
}

