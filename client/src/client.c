#include "client.h"
/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/
int main(int argc, char *argv[])
{
	int socket, port;
	char recv_buffer[MAXDATASIZE];
	struct sockaddr_in server_addr;
	struct hostent * server;
	char send_buffer[MAXDATASIZE];
	bool game_over = false;

	if (!parse_commandline_args(argc, argv, &port, &server))
	{
		printf("[ERROR]\tparse_commandline_args(int c, char **v, int * p, struct hostent ** s)\n");
		return EXIT_FAILURE;
	}

	if (!construct_socket(port, &socket, &server_addr, server))
	{
		printf("[ERROR]\tconstruct_socket(int p, int * sock, struct sockaddr_in * server, struct hostent * s)\n");
		return EXIT_FAILURE;
	}	
	signal(SIGINT, intHandler);

	if (server_status(socket,recv_buffer))
	{
		if (authentication_successfull(socket, recv_buffer))
		{
			int choice;
			while(!game_over)
			{
				choice = 0;
				printf("\n\n\n");
				printf("%s",gameheader);
				printf("%s",banner);
				scanf("%d",&choice);
	
				if (check_user_input(choice, &game_over))
				{
					clear_buffer(send_buffer);
					sprintf(send_buffer,"%d",choice);
					send_message(socket,send_buffer);
					play_game(socket, choice, recv_buffer, send_buffer);
				}
			}
		}
		else
		{
			printf("\n%s!!\n\n", recv_buffer);
		}
	}
	else
	{
		printf("\n%s\nTry Again Later!!\n\n",recv_buffer);
	}
	printf("Thank you - disconnecting\n");
	close(socket);	
	return EXIT_SUCCESS;
}

void intHandler(int sig)
{
	signal(sig, SIG_IGN);
	printf("\nCtrl+c Detected!\n");
	printf("Terminating...\n");
	exit(EXIT_SUCCESS);
}

bool server_status(int socket_id, char * buffer)
{
	receive_message(socket_id, buffer);
	if (strcmp(buffer,"success") == 0)
		return true;

	return false;
}

bool parse_commandline_args(int c, char **v, int * p, struct hostent ** s)
{
	if (c != 3)
	{
		printf("[ERROR]\tUsage: %s server_IP portnum\n",v[0]);
		return false;
	}
	*p = atoi(v[2]);
	if (*p == 0)
	{
		printf("[ERROR]\tportnum needs to be numeric\n");
		return false;
	}

	if ((*s = gethostbyname(v[1]))==NULL)
	{
		herror("gethostbyname");
		return false;
	}

	return true;
}


bool construct_socket(int p, int * sock, struct sockaddr_in * server, struct hostent * s)
{
	if ((*sock = socket(AF_INET, SOCK_STREAM, 0))== -1)
	{
		perror("scoket");
		return false;
	}

	server->sin_family = AF_INET;
	server->sin_port = htons(p);
	server->sin_addr = *((struct in_addr *)s->h_addr);
	bzero(&(server->sin_zero), sizeof(server->sin_zero));

	if (connect(*sock, (struct sockaddr *) server, sizeof(struct sockaddr)) == -1)
	{
		perror("connect");
		return false;
	}

	return true;
}


void display_homepage(char u[], char p[])
{
	printf("%s",gameheader);
	printf("%s\n",homepage);
	
	printf("Please enter your username--> ");
	scanf("%s",u);
	
	printf("Please enter your password--> ");
	get_password(p);
	
}



bool authentication_successfull(int socket_id, char * buffer)
{
	char username[CREDENTIALSIZE], password[CREDENTIALSIZE];
	char s_buffer[MAXDATASIZE];
	
	display_homepage(username, password);

	clear_buffer(s_buffer);

	sprintf(s_buffer,"%s %s",username,password);

	if (!send_message(socket_id, s_buffer)){
		return false;	
	}	
	
	if (!receive_message(socket_id, buffer)){
		return false;
	}
	
	if (strcmp(DISPLAYBANNER, buffer) == 0){
		return true;	
	}
	
	return false;
}



void play_game(int socket_id, int choice, char * recv_buffer, char * send_buffer)
{
	switch(choice)
	{
		case 1:	
			clear_buffer(send_buffer);
			receive_message(socket_id, recv_buffer);
			int oldwon=-2;
			print_game(recv_buffer, send_buffer, &oldwon, oldwon);
			send_message(socket_id,send_buffer);
			while(true)
			{
				int newwon=0;
				clear_buffer(send_buffer);
				receive_message(socket_id, recv_buffer);
				print_game(recv_buffer, send_buffer, &newwon, oldwon);

				if (strcmp(send_buffer,GAMEOVER) == 0)
				{
					strcpy(send_buffer,"Over");
					send_message(socket_id,send_buffer);
					receive_message(socket_id, recv_buffer);
					printf("\n%s\n",recv_buffer);
					break;
				}
				if (oldwon+1 == newwon)
				{
					strcpy(send_buffer,"Over");
					send_message(socket_id,send_buffer);
					receive_message(socket_id, recv_buffer);
					printf("\n%s\n",recv_buffer);
					break;
				}
				send_message(socket_id,send_buffer);
			}
			break;
		case 2:
			receive_message(socket_id, recv_buffer);
			print_ledger(recv_buffer);
			break;
	}
	return;
}



bool exit_user()
{
	char ch;
	char c;

	do
	{
		while((c = getchar()) != '\n' && c != EOF);//clear the buffer

		printf("Do you want to exit[Y/N]? ");
		scanf("%c",&ch);	
		if (ch != 'Y' && ch != 'y' && ch != 'N' && ch != 'n')
		{
			printf("Invalid choice! Try Again\n");
		}
		
	}while(ch != 'Y' && ch != 'y' && ch != 'N' && ch != 'n');

	if (ch=='Y'||ch=='y')
	{
		return true;
	}
	return false;
}



bool check_user_input(int choice, bool *game_over)
{
	char c;
	if (choice == 1 || choice == 2 || choice == 3)
	{
		if (choice==3)
		{
			if (exit_user())
			{
				*game_over = true;
			}
			else
			{
				*game_over = false;
				return false;
			}	
		}
		return true;
	}
	else
		printf("\n\n\n[Invalid choice!]\n\n\n");
	while((c = getchar()) != '\n' && c != EOF);//clear the buffer
	return false;
}



void print_ledger(char * message)
{
	char * token = strtok(message, ",");
	while(token){
		char s[10];
		int game_won, game_played;
		sscanf(token,"%s %d %d",s, &game_won, &game_played);
    	printf("\n%s\n",thick_line);
    	printf("Player Name: %s\n", s);
    	printf("Game Won: %d\n", game_won);
    	printf("Games Played: %d\n", game_played);
    	printf("%s\n",thick_line);
    	token = strtok(NULL,",");
	}
}

void print_game(char * message, char * ch, int * won, int oldwon)
{
	char first[CREDENTIALSIZE], second[CREDENTIALSIZE];
	char guessed[MAXIMUMGUESS];
	int guess;
	int i=0;
	sscanf(message,"%d %d %s %s %s", &guess, won, first, second, guessed);
	
	if (strcmp(guessed,"EMPTYEMPTYEMPTY") == 0){
		printf("\nGuessed letters: \n\n");
	}
	else{	
		printf("\nGuessed letters: %s\n\n",guessed);
	}

	printf("Number of guesses left: %d\n\n",guess);
	printf("Word: ");
	while(first[i] != '\0')
	{
		printf("%c ",first[i]);
		i++;
	}
	i=0;
	printf("  ");
	while(second[i] != '\0')
	{
		printf("%c ",second[i]);
		i++;
	}
	printf("\n\n");
	if (guess > 0 && oldwon+1 != *won)
	{
		printf("Enter your guess - ");
		char c;
		while((c = getchar()) != '\n' && c != EOF);//clear the buffer	 
		scanf("%c",&c);
		strcpy(ch,&c);
		ch[1]='\0';
	}
	else
	{
		printf("Game Over\n");
		strcpy(ch,GAMEOVER);
	}

	printf("\n%s\n\n",thin_line);
}
