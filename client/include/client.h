#ifndef CLIENT_H
#define CLIENT_H

#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>

#include "engine.h"

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

#define h_addr h_addr_list[0]
#define DISPLAYBANNER "display_banner"

#define CREDENTIALSIZE 20
#define MAXIMUMGUESS 26
#define GAMEOVER "game_over"

const char * banner =	  "Please enter a selection\n"
			  			  "<1> Play Hangman\n"
	     		  		  "<2> Show Leaderboard\n"
			  			  "<3> Quit\n"
		          		  "\n"
		          		  "Selection option 1-3 ->";

const char * thick_line = "=============================================";

const char * thin_line =  "---------------------------------------------";


const char * gameheader = "=============================================\n"
			   			  "\n"
			   			  " Welcome to the online Hangman Gaming system \n"
			   			  "\n"
			   			  "=============================================\n";

const char * homepage =   "\n"
		          		  "\n"
		          		  "[You are required to login first]\n";
void intHandler(int sig);
bool server_status(int socket_id, char * buffer);
bool parse_commandline_args(int c, char ** v, int * p, struct hostent ** s);
bool construct_socket(int p, int * sock, struct sockaddr_in * server, struct hostent * s);
void display_homepage(char u[], char p[]);
bool authentication_successfull(int socket_id, char * buffer);
void play_game(int socket_id, int choice, char * recv_buffer, char * send_buffer);
bool exit_user();
bool check_user_input(int choice, bool *game_over);
void print_ledger(char * message);
void print_game(char * message, char * ch, int * won, int oldwon);

#endif
