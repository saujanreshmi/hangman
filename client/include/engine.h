#ifndef ENGINE_H
#define ENGINE_H

#include <sys/socket.h>
#include <sys/types.h>
#include <termios.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <unistd.h>

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

#define MAXDATASIZE 255

void get_password(char * password);
void clear_buffer(char * message);
bool receive_message(int socket_id, char * buffer);
bool send_message(int socket_id, char * message);


#endif
