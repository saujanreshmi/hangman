#define _GNU_SOURCE
#include "server.h"

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

pthread_mutex_t request_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_cond_t  got_request   = PTHREAD_COND_INITIALIZER;

sem_t r_mutex, w_mutex;

int main(int argc, char *argv[])
{
	int socket_fd, new_fd;
	struct sockaddr_in my_addr;    
	struct sockaddr_in their_addr; 
	socklen_t sin_size;
	int port;
	time_t t;
	pthread_t pthread[USERS];
	int thid[USERS];

	hash_t login_credentials; //hash table to store login details
	dict_t * hangman_text; //linked list to store hangman_text
	player_t * client = NULL; //linked list to store active players

	sem_init(&r_mutex, 0, 1);
	sem_init(&w_mutex, 0, 1);
	srand((unsigned) time(&t));
	
	if (!parse_commandline_args(argc, argv[1], &port)){
		printf("[ERROR]\tparse_commandline_args(int c, char *v, int * port)\n");
		return EXIT_FAILURE;
	}

	if (!construct_socket(&socket_fd, &my_addr, port)){
		printf("[ERROR]\tconstruct_socket(int * socket, struct sockaddr_in * m_addr, int port)\n");
		return EXIT_FAILURE;
	}

	if (!initialise_variables(&login_credentials, &hangman_text, &client)){
		printf("[ERROR]\tinitialise_variables(hash_t * h, dict_t ** d)\n");
		return EXIT_FAILURE;
	}

	for(int i =0 ; i <USERS; i++)
	{
		thid[i] = i;
		if (pthread_create(&pthread[i], NULL, handle_request, (void *)&thid[i]) != 0){
			printf("[ERROR]\tfailure in threads creation\n");
			return EXIT_FAILURE; //error occured
		}
	}

	signal(SIGINT, intHandler);

	while (true)
	{
		sin_size = sizeof(struct sockaddr_in);
		if ((new_fd = accept(socket_fd, (struct sockaddr *)&their_addr, &sin_size)) == -1) {
			perror("accept");
			continue;
		}
		//printf("Number of clients: %d",clients);
		if (clients < USERS)
		{
			send_message(new_fd,"success");
			if (!add_request(inet_ntoa(their_addr.sin_addr), new_fd, &client, &login_credentials, hangman_text, &request_mutex, &got_request))
			{
				printf("[ERROR]\tadd_request(char * ip, int sock, player_t **p, hash_t *l, dict_t * d, pthread_mutex_t * p_mutex, pthread_cond_t * p_cond_var)\n");
				return EXIT_FAILURE; //error occured
			}
	
		}
		else
		{
			send_message(new_fd,server_busy);
			close(new_fd);
			printf("[ROOT THR]\t%s: Connection rejected for %s\n", server_prompt, inet_ntoa(their_addr.sin_addr));
		}
	}
	return EXIT_SUCCESS;
}

void intHandler(int sig)
{
	signal(sig, SIG_IGN);

	destroy_player(client);
	destroy_dict(hangman_text);
	destroy_hash(&login_credentials);
	destroy_request();
	pthread_mutex_destroy(&request_mutex);
	printf("\n[ROOT THR]\tCtrl+c Detected!\n");
	printf("[ROOT THR]\t%s: Terminating...\n",server_prompt);
	exit(EXIT_SUCCESS);
}



bool parse_commandline_args(int c, char *v, int * port){
	if (c == 1){
		*port = DEFAULT_PORT;
	}
	else if (c == 2){
		*port = atoi(v);
		if (*port == 0){
			printf("[WARNING]\tportnum should be numeric default port is used\n\n");
			*port = DEFAULT_PORT;
		}
	}
	else {
		printf("[ERROR]\tUsage: ./server portnum\n");
		return false;

	}

	return true;
}



bool construct_socket(int * sock, struct sockaddr_in * m_addr, int port){
	if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		return false;
	}

	m_addr->sin_family = AF_INET;         /* host byte order */
	m_addr->sin_port = htons(port);     /* short, network byte order */
	m_addr->sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */

	/* bind the socket to the end point */
	if (bind(*sock, (struct sockaddr *)m_addr, sizeof(struct sockaddr)) == -1) {
		perror("bind");
		return false;
	}

	/* start listnening */
	if (listen(*sock, USERS) == -1) {
		perror("listen");
		return false;
	}

	printf("[ROOT THR]\tHangman Server started...\n");
	printf("[ROOT THR]\tListening in %d...\n",port);
	return true;
}



bool initialise_variables(hash_t * h, dict_t ** d, player_t ** p)
{
	char authentication_file[1024], hangman_file[1024];

	if (!find_location("Authentication.txt",authentication_file))
		return false;
	if(!find_location("hangman_text.txt",hangman_file))
		return false;

	if (!init_hash(h,USERS)){
		return false;
	}

	if (!init_dict(d)){
		return false;
	}
	if (!init_player(p)){
		return false;	
	}
	if (!parse_login_credentials_from_file(authentication_file,h)){
		return false;
	}
	
	if (!parse_hangman_text_from_file(hangman_file, d)){
		return false;	
	}
	return true;
}




