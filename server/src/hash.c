#include "hash.h"

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

bool init_hash(hash_t * h, size_t n)
{
	h->size = n;
	h->buckets = (dict_t **) calloc(n, sizeof(dict_t *));

	if (!h->buckets){
		perror("calloc");
		return false;
	}

	return true;
}



size_t djb_hash(char * s)
{
	size_t hash_value = 6589;
	int c;

	while ((c = *s++) != '\0')
	{
		hash_value = ((hash_value << 5) + hash_value) + c;
	}
	return hash_value;
}



size_t index_hash(const hash_t * h, char * key)
{
	return djb_hash(key) % h->size;
}



dict_t * bucket_hash(const hash_t * h, char * key) 
{
	return h->buckets[index_hash(h, key)];
}



bool add_hash(hash_t * h, char * key, char * value)
{
	dict_t * newhead = (dict_t *) malloc(sizeof(dict_t));
	if (!newhead)//NULL
	{
		return false;
	}
	
	allocate_space(newhead, key, value);

	size_t bucket = index_hash(h, key);
	newhead->next = h->buckets[bucket];
	h->buckets[bucket] = newhead;

	return true;		
}



bool contains_hash(const hash_t * h, char * key, char * value)
{
	for(dict_t * i = bucket_hash(h, key); i != NULL; i = i->next)
	{
		if ((strcmp(i->key, key) == 0) && (strcmp(i->value, value) == 0))
		{
			return true;
		}
	}

	return false;
}

void print_hash(hash_t * h) {
    printf("hash table with %d buckets\n", (int)h->size);
    for (size_t i = 0; i < h->size; ++i) {
        printf("bucket %d: ",(int)i);
        if (h->buckets[i] == NULL) {
            printf("empty\n");
        } else {
            for (dict_t *j = h->buckets[i]; j != NULL; j = j->next) {
                item_print(j);
                if (j->next != NULL) {
                    printf(" -> ");
                }
            }
            printf("\n");
	}
}
}

void item_print(dict_t * i)
{
	printf("Key=%s value=%s",i->key, i->value);
}

void destroy_hash(hash_t *h)
{
	 for (size_t i = 0; i < h->size; ++i) {
	 		destroy_dict(h->buckets[i]);
	 }
	 free(h->buckets);
}
