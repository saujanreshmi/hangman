#include "thpool.h"
/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

request_t * head = NULL;
request_t * tail = NULL;
int requests = 0;//counts the incoming requests

void * handle_request(void * id)
{
	int rc;
	request_t * new_request;
	int thread_id = *((int *)id);
	if ((rc = pthread_mutex_lock(&request_mutex)) != 0 )
		printf("[WARNING]\tCannot lock the mutex [errono: %d]\n",rc);

	while(true)
	{
		if (requests > 0)
		{
			new_request = get_request(&request_mutex);
			//printf stuff
			//printf("[THREAD %d]\tRequest Available\n",thread_id);
			if (new_request)
			{
				if ((rc = pthread_mutex_unlock(&request_mutex)) != 0 )
					printf("[WARNING]\tCannot unlock the mutex [errono: %d]\n",rc);
		
				printf("[THREAD %d]\tHangman Server: got connection from %s in socket:%d\n", thread_id, new_request->ip, new_request->socket_id);
				
				if (!handle_player(new_request->socket_id, new_request->l, new_request->p, new_request->d, thread_id))
					printf("[THREAD %d]\tHangman Server: lost connection from %s\n",thread_id, new_request->ip);
				else
					printf("[THREAD %d]\tHangman Server: connection dropped by %s\n",thread_id, new_request->ip);
				clients--;
				free(new_request);
				close(new_request->socket_id);
				/*
				if ((rc = pthread_mutex_unlock(&request_mutex)) != 0 )
					printf("[WARNING]\tCannot unlock the mutex [errono: %d]\n",rc);
				*/
				if ((rc = pthread_mutex_lock(&request_mutex)) != 0 )
					printf("[WARNING]\tCannot lock the mutex [errono: %d]\n",rc);
			}
		}else
		{
			printf("[THREAD %d]\tWaiting for the request\n",thread_id);
			if ((rc = pthread_cond_wait(&got_request, &request_mutex)) != 0)
				printf("[WARNING]\tCannot block the thread [errono: %d]\n",rc);
		}
	}
}



bool add_request(char * ip, int sock, player_t **p, hash_t *l, dict_t * d, pthread_mutex_t * p_mutex, pthread_cond_t * p_cond_var)
{
	int rc;
	request_t * tmp;

	tmp = (request_t *) malloc(sizeof(request_t));
	if (!tmp){
		perror("malloc");
		return false;
	}
	strcpy(tmp->ip,ip);
	tmp->socket_id = sock;
	tmp->p = p;
	tmp->l = l;
	tmp->d = d;
	tmp->next = NULL;
	
	if ((rc = pthread_mutex_lock(p_mutex)) != 0 )
		printf("[WARNING]\tCannot lock the mutex [errono: %d]\n",rc);

	if (requests == 0){
		head = tmp;
		tail = tmp;
	}
	else {
		tail->next = tmp;
		tail = tmp;
	}

	requests++;
	clients++;
	//printf("Requests:%d Got new request from %s in Socket: %d\n",requests, tmp->ip, tmp->socket_id);


	if ((rc = pthread_mutex_unlock(p_mutex))!= 0 )
		printf("[WARNING]\tCannot unlock the mutex [errono: %d]\n",rc);

	if ((rc = pthread_cond_signal(p_cond_var))!= 0)
		printf("[WARNING]\tCannot signal the thread [errono: %d]\n",rc);

	return true;

}


request_t * get_request(pthread_mutex_t * p_mutex)
{
	int rc;
	request_t * tmp;

	if ((rc = pthread_mutex_lock(p_mutex))!= 0 )
		printf("[WARNING]\tCannot lock the mutex [errono: %d]\n",rc);

	if (requests > 0){
		tmp = head;
		head= head->next;
		if (head == NULL){
			tail = NULL;
		}
		requests--;
	}
	else{
		tmp = NULL;
	}

	if ((rc = pthread_mutex_unlock(p_mutex)) != 0 )
		printf("[WARNING]\tCannot unlock the mutex [errono: %d]\n",rc);

	return tmp;

}

void destroy_request()
{
	request_t * tmp = head;
	if (tmp != NULL)
	{	
		head = head->next;
		free(tmp);
		tmp = head;
	}
}
