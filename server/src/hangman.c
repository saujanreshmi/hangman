#include "hangman.h"
/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

bool init_hangman(hangman_t ** h)
{
	sem_wait(&w_mutex);
	*h = malloc(sizeof(hangman_t));
	if (!(*h))
	{
		perror("malloc");
		return false;
	}
	(*h)->word = NULL;
	(*h)->guess_left = 0;
	(*h)->guessed_letter = NULL;
	(*h)->played = 0;
	(*h)->won = 0;
	sem_post(&w_mutex);
	return true;
}

bool add_guessed_letter(hangman_t ** h, char * ch)
{
	sem_wait(&w_mutex);
	strcat((*h)->guessed_letter,ch);
	sem_post(&w_mutex);
	return true;
}

void increment_won(hangman_t ** h)
{
	sem_wait(&w_mutex);

	(*h)->won++;

	sem_post(&w_mutex);
}

void increment_played(hangman_t ** h)
{

	sem_wait(&w_mutex);

	(*h)->played++;

	sem_post(&w_mutex);
}

void decrement_guess_left(hangman_t ** h)
{
	sem_wait(&w_mutex);

	(*h)->guess_left--;

	sem_post(&w_mutex);
}

bool change_hangman(hangman_t ** h, char * word)
{
	sem_wait(&w_mutex);
	char first[20], second[20];

	(*h)->word = realloc((*h)->word, sizeof(char)*strlen(word));
	if (!(*h)->word)
	{
		perror("malloc");
		return false;
	}
	memcpy((*h)->word,(char *)word,strlen(word));

	sscanf(word,"%s %s",first, second);

	(*h)->guess_left = MIN(strlen(first)+strlen(second)+10,26);

	(*h)->guessed_letter = realloc((*h)->guessed_letter,sizeof(char)*(*h)->guess_left);
	memset((*h)->guessed_letter,'\0',sizeof(char)*(*h)->guess_left);

	sem_post(&w_mutex);
	return true;
}

bool randomise_word_hangman(dict_t * list_word, char ** word)
{	

	int i = 0, random;
	random = rand()%length_dict(list_word);
	char buffer[40];	

	while(true)
	{
		if (i == random)
		{	
			*word = malloc((sizeof(char)*strlen(list_word->key))+(sizeof(char)*strlen(list_word->value))+1);
			if (!(*word))
			{
				perror("malloc");
				return false;	
			}
			sprintf(buffer,"%s %s",list_word->key,list_word->value);
			memcpy(*word,(char*)buffer,strlen(buffer));
			break;		
		}
		i++;
		list_word=list_word->next;
	}

	return true;
}

bool guess_left(hangman_t * h)
{
	if (h->guess_left <= 0)
		return true;
	return false;
}

void destroy_hangman(hangman_t * h)
{
	sem_wait(&w_mutex);
	free(h->word);
	free(h->guessed_letter);
	free(h);
	sem_post(&w_mutex);
}
