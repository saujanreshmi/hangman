#include "engine.h"
/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

bool find_location(const char * name, char buffer[])
{

	if (getcwd(buffer, 1024) != NULL)
	{
		char * buf =  (char *)malloc(sizeof(char)*1024);
		strcpy(buf,buffer);
		//memcpy(buf, (char *)buffer, strlen(buffer));
		strcpy(buffer,buf);
		strcat(buffer,"/");
		strcat(buffer,name);
		free(buf);
		return true;
	}
	else
	{
		perror("getcwd");
		return false;	
	}
}

//this function works remove this comment during submission
bool parse_login_credentials_from_file(const char * file_location, hash_t * login_credential)
{
	FILE * fp = fopen(file_location, "r");
	char line[100];
	char username[CREDENTIALSIZE], password[CREDENTIALSIZE];
	
	if (!fp)
	{
		perror("Canot open file");
		return false;
	}

	while( fgets(line, sizeof(line), fp) != NULL)
	{
		sscanf(line, "%s %s", username, password);

		if (strcmp(username, "Username") != 0)
		{
			if ( ! add_hash(login_credential, username, password) ) //error when adding to hash
			{
				printf("Cannot add username and password to hash");
				return false;
			}
		}
		else
		{
			continue;
		}
	}
	fflush(fp);
	return true;
}


//this function works remove this comment during submission
bool parse_hangman_text_from_file(const char * file_location, dict_t ** hangman_text)
{
	FILE * fp = fopen(file_location, "r");; 
	char line[100];
	const char s[2] =",";
	char * token;
	char first_word[CREDENTIALSIZE], second_word[CREDENTIALSIZE];

	if (!fp)
	{
		perror("Canot open file");
		return false;
	}

	while( fgets(line, sizeof line, fp) != NULL)
	{

		token = strtok(line, s);
		strcpy(first_word, token);
			
		while(token)
		{
			token = strtok(NULL, s);
			break;
		}
		strcpy(second_word, token);
		second_word[strlen(second_word)-1]=0;

		if (!add_dict(hangman_text, first_word, second_word))
		{
			return false;
		}
	}
	fflush(fp);
	return true;
}


bool authenticate(const hash_t * credentials, char * username, char * password)
{
	return contains_hash(credentials, username, password);
}


void clear_buffer(char * buffer)
{
	memset(buffer, 0, MAXDATASIZE);
}


bool receive_message(int socket_id, char * buffer)
{
	int numbytes;
	clear_buffer(buffer);
	if ((numbytes= recv(socket_id, buffer, MAXDATASIZE, 0)) == -1){
		perror("recv");
		return false;
	}
	else if (numbytes == 0){
		return false;	
	}
	buffer[numbytes] = '\0';
	return true;
}


bool send_message(int socket_id, char * message)
{
	if (send(socket_id, message, strlen(message),0) == -1){
		perror("send");
		return false;
	}

	return true;
}


bool play_game(int socket_id, int choice, char * buffer, player_t **p, player_t * head, dict_t * list_word, int thid)
{
	clear_buffer(buffer);
	if (choice == 1 || choice == 2)
	{
		switch(choice)
		{
			case 1:

				clear_player(*p);
				char * w;

				if (!randomise_word_hangman(list_word, &w))
					return false;
				
				
				
				if (!change_hangman(&((*p)->game), w))
					return false;
				

				bool player_guessed = false;
				while(true)
				{
					clear_buffer(buffer);
					format_word(*p, buffer, ENCRYPT, &player_guessed, thid);

					send_message(socket_id,buffer);
					
					
					if (guess_left_player(*p))
					{

						increment_played(&((*p)->game));
						if(!receive_message(socket_id,buffer)){
							return false;
						}
						clear_buffer(buffer);
						char * name = malloc(20*sizeof(char));
						get_player_name(*p, name);
						sprintf(buffer,"Bad luck %s! You have run out of guesses. The Hangman got you!",name);
						free(name);
						send_message(socket_id, buffer);
						break;
					}
					if(player_guessed)
					{
							//printf("STUCK\n");
							if(!receive_message(socket_id,buffer)){
								return false;
							}
							clear_buffer(buffer);
							char * name = malloc(20*sizeof(char));
							get_player_name(*p, name);
							sprintf(buffer,"Well done %s! You have won this round of the Hangman!",name);
							free(name);
							//printf("%s\n",buffer);
							send_message(socket_id, buffer);
							break;
						
					}
					
					//printf("STUCK\n");
					if(!receive_message(socket_id,buffer)){
						return false;
					}
					
					add_guessed_letter(&((*p)->game),buffer);
					decrement_guess_left(&((*p)->game));
						
				}
				(*p)->active = false;
				break;
			case 2:
				construct_ledger(head, buffer);
				send_message(socket_id, buffer);
				break;

			default:
				perror("Unexpected input");
				return false;
		}
		return true;
	}
	return false;
}

void construct_ledger(player_t *p, char * buffer)
{
	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);
	char tmp[20];

	for (; p != NULL; p=p->next)
	{
		sprintf(tmp,"%s %d %d,",p->name,p->game->won, p->game->played);
		strcat(buffer,tmp);
		memset(tmp,0,sizeof(tmp));
	}
	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);
	return;
}


void format_word(player_t * p, char * buffer, char replace, bool * check, int thid)
{
	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);
	int i=0;
	char str[MAXDATASIZE];
	*check = false;
	strcpy(str,p->game->word);
	printf("[THREAD %d]\t%s\n",thid, str);

	while(str[i] != '\0')
	{
		if (!strchr(p->game->guessed_letter,str[i]) && str[i] != ' ')
		{
			str[i] = replace;
		}
		i++;
	}
	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);

	if (same_player_word(str,p))
	{
		//printf("Matched\n");
		increment_won(&(p->game));
		increment_played(&(p->game));
		*check = true;
	}
	//printf("%s\n",str); 
	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);

	if (strcmp(p->game->guessed_letter, "") != 0)
		sprintf(buffer,"%d %d %s %s", p->game->guess_left, p->game->won, str,p->game->guessed_letter);
	else
		sprintf(buffer,"%d %d %s EMPTYEMPTYEMPTY", p->game->guess_left, p->game->won, str);

	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);

	
}

bool handle_player(int socket_id, const hash_t * credentials, player_t **p, dict_t * text, int thid)
{
	char buffer[MAXDATASIZE];
	char username[20], password[20];
	int choice;
	player_t * current_p = NULL;

	if (!receive_message(socket_id, buffer)){
		return false;
	}

	sscanf(buffer,"%s %s",username,password);

	if(authenticate(credentials,username,password)){
		
		if (contains_player(*p, username)){
			//printf("Player already exists\n");
			current_p = get_player(*p, username);
			if (is_player_active(current_p))
			{
				send_message(socket_id,"Hangman Server: This user is already logged-in");
				return false;
			}
			set_player_active(current_p);
		}
		else{
			current_p = add_player(p, username);//add new player
			if (!init_hangman(&current_p->game))
			{
				printf("%s: Unexpected error\n",server_prompt);
				return false;
			}
		}
		
		if (!send_message(socket_id, success_authentication)){

			return false;
		}
		
		sem_wait(&r_mutex);
		sem_wait(&w_mutex);
		sem_post(&r_mutex);
		if (current_p == NULL)
		{
			printf("%s: Unexpected error\n",server_prompt);
			sem_wait(&r_mutex);
			sem_post(&w_mutex);
			sem_post(&r_mutex);
			return false;
		}
		sem_wait(&r_mutex);
		sem_post(&w_mutex);
		sem_post(&r_mutex);

		char *name = malloc(20* sizeof(char));
		get_player_name(current_p, name);
		printf("[THREAD %d]\t%s: Logged in user: %s\n",thid ,server_prompt, name);
		free(name);
		while(true)
		{
			clear_buffer(buffer);
			//printf("WAITING FOR MESSAGE\n");
			if(!receive_message(socket_id,buffer)){
				current_p->active= false;
				break;
			}
			printf("[THREAD %d]\t%s: User selected: %s\n",thid, server_prompt, buffer);

			choice = atoi(buffer);
			
			if (!play_game(socket_id, choice, buffer, &current_p, *p, text, thid))
			{
				set_player_inactive(current_p);
				break;
			}

		}
		
		return true;
	}

	else{
		send_message(socket_id,failed_authentication);
		return false;
	}
	
}

