#include "player.h"
#define RAW_PLAYER "EMPTY"

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

bool init_player(player_t ** p)
{
	sem_wait(&w_mutex);
	*p = (player_t *) malloc(sizeof(player_t));
	if (!(*p))
	{
		perror("malloc");
		sem_post(&w_mutex);
		return false;
	}
	(*p)->name = RAW_PLAYER;
	(*p)->next = NULL;
	(*p)->game = NULL;
	(*p)->active = true;
	sem_post(&w_mutex);

	return true;
}

player_t * add_player(player_t ** p, char * name)
{	
	if (strcmp(RAW_PLAYER, (*p)->name) == 0)
	{
		sem_wait(&w_mutex);		
		(*p)->name = malloc(sizeof(char)*strlen(name));
		if(!(*p)->name){
			perror("malloc");
			sem_post(&w_mutex);
			return NULL;	
		}
		memcpy((*p)->name,(char *)name,strlen(name));
		(*p)->game = NULL;
		(*p)->next = NULL;
		sem_post(&w_mutex);
		return *p;
	}
	
	player_t * newplayer = NULL;

	if (init_player(&newplayer))
	{
		sem_wait(&w_mutex);
		newplayer->name = malloc(sizeof(char)*strlen(name));
		if(!newplayer->name){
			perror("malloc");
			sem_post(&w_mutex);
			return false;	
		}
		memcpy(newplayer->name,(char *)name,strlen(name));
		newplayer->game = NULL;
		newplayer->next = NULL;

		newplayer->next = *p;
		*p = newplayer;
		sem_post(&w_mutex);

		return *p;
	}
	else
	{
		return NULL;
	}
}


bool contains_player(player_t * p, char * name)
{
	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);
	for(; p != NULL; p=p->next)
	{
		if (strcmp(name, p->name) == 0)
		{
			sem_wait(&r_mutex);
			sem_post(&w_mutex);
			sem_post(&r_mutex);
			return true;
		}
	}
	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);
	return false;
}


player_t * get_player(player_t *p, char * username)
{
	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);
	for(; p != NULL; p=p->next)
	{
		if (strcmp(username, p->name) == 0)
		{
			sem_wait(&r_mutex);
			sem_post(&w_mutex);
			sem_post(&r_mutex);
			return p;
		}
	}
	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);

	return NULL;
}

int count_player(player_t * p, bool active)
{
	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);
	
	int count=0;
	
	for(; p != NULL; p=p->next)
	{	
		if (p->active && active)
		{
			count++;
		}
		if (!active)
		{
			count++;
		}
	}

	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);

	return count;
}

void clear_player(player_t *p)
{	
	sem_wait(&w_mutex);
	p->game->word = NULL;
	p->game->guessed_letter = NULL;
	p->game->guess_left = 0;
	sem_post(&w_mutex);
}

bool guess_left_player(player_t *p)
{
	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);
	
	bool yes;
	yes = guess_left(p->game);

	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);

	return yes;
}

void get_player_name(player_t *p, char * n)
{
	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);
	strcpy(n,p->name);
	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);
}

bool same_player_word(char * s, player_t * p)
{
	bool yes = false;

	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);

	if (strcmp(s,p->game->word) == 0)
		yes = true;

	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);

	return yes;
}

bool is_player_active(player_t * p)
{
	sem_wait(&r_mutex);
	sem_wait(&w_mutex);
	sem_post(&r_mutex);
	if (p->active)
	{
		sem_wait(&r_mutex);
		sem_post(&w_mutex);
		sem_post(&r_mutex);
		return true;
	}
	sem_wait(&r_mutex);
	sem_post(&w_mutex);
	sem_post(&r_mutex);
	return false;
}

void set_player_active(player_t *p)
{
	sem_wait(&w_mutex);
	p->active = true;
	sem_post(&w_mutex);
}

void set_player_inactive(player_t *p)
{
	sem_wait(&w_mutex);
	p->active = false;
	sem_post(&w_mutex);
}

void destroy_player(player_t *p)
{
	sem_wait(&w_mutex);
	player_t * tmp;

	while(p != NULL)
	{
		tmp = p;
		p = p->next;
		if (strcmp(tmp->name,RAW_PLAYER) != 0)free(tmp->name);
		if (tmp->game) destroy_hangman(tmp->game);
		free(tmp);
	}
	sem_post(&w_mutex);
}
