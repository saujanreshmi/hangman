#include "dictionary.h"
/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/
#define RAW_KEY "EMPTY"

bool init_dict(dict_t ** d)
{
	*d = (dict_t *) malloc(sizeof(dict_t));

	if (!(*d))
	{
		perror("malloc");
		return false;
	}
	(*d)->key = RAW_KEY;
	(*d)->value = "";
	(*d)->next = NULL;

	return true;
}


bool add_dict(dict_t ** d, char * k, char * v)
{
	if (strcmp(RAW_KEY, (*d)->key) == 0)
	{
		if (!allocate_space(*d, k, v))
		{
			return false;
		}
		return true;
	}
	
	dict_t * newnode = NULL;

	if (init_dict(&newnode))
	{
		if (!allocate_space(newnode, k, v))
		{
			return false;
		}

		newnode->next = *d;
		*d = newnode;

		return true;
	}
	else
	{
		return false;
	}

}

int length_dict(dict_t * d)
{
	int count=0;
	for(; d != NULL; d=d->next)
	{
		if (strcmp(RAW_KEY, d->key) != 0)
		{
			count++;
		}
	}

	return count;
}

void destroy_dict(dict_t *d )
{
	dict_t * tmp;

	while(d != NULL)
	{
		tmp = d;
		d = d->next;
		free(tmp->key);
		free(tmp->value);
		free(tmp);
	}
}

bool allocate_space(dict_t *d, char * k, char *v)
{
	d->key = malloc(sizeof(char)*strlen(k));
	d->value = malloc(sizeof(char)*strlen(v));
	
	if(!d->key || !d->value){
		perror("malloc");
		return false;	
	}

	memcpy(d->key,(char *)k,strlen(k));
	memcpy(d->value,(char *)v,strlen(v));

	return true;
}
