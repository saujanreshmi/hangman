#ifndef SERVER_H
#define SERVER_H

#include <netinet/in.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>
#include <semaphore.h>

#include "engine.h"
#include "thpool.h"

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

#define USERS 10
#define DEFAULT_PORT 12345
#define _BSD_SOURCE

hash_t login_credentials; //hash table to store login details
dict_t * hangman_text; //linked list to store hangman_text
player_t * client = NULL; //linked list to store active players
int clients = 0;

/* Function prototypes */
void intHandler(int sig);
bool parse_commandline_args(int c, char *v, int * port);
bool construct_socket(int * socket, struct sockaddr_in * m_addr, int port);
bool initialise_variables(hash_t * h, dict_t ** d, player_t ** p);


#endif
