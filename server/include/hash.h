#ifndef HASH_H
#define HASH_H

#include "dictionary.h"

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

typedef struct hash
{
	dict_t ** buckets;
	size_t size;
}hash_t;

bool init_hash(hash_t * h, size_t n);

size_t djb_hash(char * s);

size_t index_hash(const hash_t * h, char * key);

dict_t * bucket_hash(const hash_t * h, char * key);

bool add_hash(hash_t * h, char * key, char * value);

bool contains_hash(const hash_t * h, char * key, char * value);

void print_hash(hash_t * h);

void item_print(dict_t * j);

void destroy_hash(hash_t * h);

#endif
