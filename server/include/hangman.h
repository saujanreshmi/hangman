#ifndef HANGMAN_H
#define HANGMAN_H

#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

#include "dictionary.h"
/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

typedef struct hangman
{
	char * word;
	char * guessed_letter;
	int guess_left;
	int played;
	int won;
}hangman_t;

extern sem_t r_mutex;
extern sem_t w_mutex;

bool init_hangman(hangman_t ** h);

bool add_guessed_letter(hangman_t ** h, char * ch);

void increment_won(hangman_t ** h);

void increment_played(hangman_t ** h);

void decrement_guess_left(hangman_t ** h);

bool change_hangman(hangman_t ** h, char * word);

bool randomise_word_hangman(dict_t * d, char ** word);

bool guess_left(hangman_t * h);

void destroy_hangman(hangman_t * h);


#endif
