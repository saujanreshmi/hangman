#ifndef PLAYER_H
#define PLAYER_H

#include <sys/types.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>

#include "hangman.h"

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

typedef struct player player_t;

struct player
{
	char * name;
	hangman_t * game;
	player_t * next;
	bool active;
};

extern sem_t r_mutex;
extern sem_t w_mutex;

bool init_player(player_t **p);

bool contains_player(player_t * p, char * name); //this function will be usefull when avoiding redundency on authorizing playersvoid clear_player(player_t *p)

player_t * add_player(player_t ** p, char * name);

player_t * get_player(player_t * p, char * username);

int count_player(player_t * p, bool active);

void clear_player(player_t *p);

bool guess_left_player(player_t *p);

void get_player_name(player_t *p, char * n);

bool same_player_word(char * s, player_t * p);

bool is_player_active(player_t * p);

void set_player_active(player_t *p);

void set_player_inactive(player_t *p);

void destroy_player(player_t *p);

#endif
