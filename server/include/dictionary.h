#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

typedef struct dict dict_t;

struct dict
{
	char * key;
	char * value;
	dict_t * next;
};

bool init_dict(dict_t ** d);

bool add_dict(dict_t ** d, char * k, char * v);

int length_dict(dict_t * d);

void destroy_dict(dict_t * d);

bool allocate_space(dict_t *d, char * k, char *v);

#endif
