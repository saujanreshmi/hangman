#ifndef THPOOL_H
#define THPOOL_H

#include <pthread.h>

#include "dictionary.h"
#include "hash.h"
#include "player.h"
#include "engine.h"

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

#define IP_LENGTH 30

typedef struct request request_t;

struct request{
	char ip[IP_LENGTH];
	int socket_id;
	player_t ** p;
	hash_t * l;
	dict_t * d;
	request_t * next;
};

extern pthread_mutex_t request_mutex;
extern pthread_cond_t  got_request;
extern int clients;

//this function is passed to pthread_create with argument thread id
void * handle_request(void * id);

//this is called every time server accepts the client and updates the linked list of request
bool add_request(char * ip, int sock, player_t **p, hash_t *l, dict_t * d, pthread_mutex_t * p_mutex, pthread_cond_t * p_cond_var);

//this is called from handle_request to get top most request
request_t * get_request(pthread_mutex_t * p_mutex);

void destroy_request();

#endif
