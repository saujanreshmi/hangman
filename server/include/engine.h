#ifndef ENGINE_H
#define ENGINE_H
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <semaphore.h>

#include "hash.h"
#include "player.h"

/*
	@Author: Saujan Thapa & Saad Usman
	@Student number: n9003096 & n8729824
	@Email: saujan.thapa@connect.qut.edu.au
		saad.usman@connect.qut.edu.au
*/

#define MAXDATASIZE 255
#define CREDENTIALSIZE 20

#define ENCRYPT '_'

#define success_authentication "display_banner"
#define failed_authentication "Hangman Server: Invalid username or password"
#define failed_connection "Maximum number of client has reached"
#define server_prompt "Hangman Server"
#define server_busy "Hangman Server: Server is Busy [Maximum players reached]"

extern sem_t r_mutex;
extern sem_t w_mutex;

bool find_location(const char * name, char buffer[]);

bool parse_login_credentials_from_file(const char * file_location, hash_t * login_credential);

bool parse_hangman_text_from_file(const char * file_location, dict_t ** hangman_text);

bool authenticate(const hash_t * credentials, char * username, char * password);

void clear_buffer(char * message);

bool receive_message(int socket_id, char * buffer);

bool send_message(int socket_id, char * message);

bool play_game(int socket_id, int choice, char * buffer, player_t **p, player_t * head, dict_t *list_word, int thid);

void construct_ledger(player_t * head, char * buffer);

void format_word(player_t * p, char * buffer, char replace, bool * check, int thid);

bool handle_player(int socket_id, const hash_t * credentials, player_t **p, dict_t * text, int thid);

#endif
