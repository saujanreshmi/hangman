#############################################################################
How to compile hangman?
This program is client server based word guessing game.
Therefore, it has two two sub programs known as server and client.

	How to compile server?
	1.	Change to server directory in the hangman directory
	2.	Run make command to create executable using "make"
	How to compile client?
	1.	Change to client directory int the hangman directory
	2.	Run make command to create executable using "make"


#############################################################################
How to play hangman?
Playing hangman involves running two executables, server and client.
However running server first is crucial.

	How to run server?
	1. 	Navigate to server directory in the hangman directory
	2. 	Run server using command "./server port_num" or "./server"

	[Note: ./server runs server in default port which is 12345
	 	and ./server port_num runs in provided port]

	E.g: ./server 9876 "This command runs server in port 9876"


	How to run client?
	1. 	Navigate to client directory in the hangman directory
	2.	Run client using command "./client server_ip server_port"
	
	E.g: ./client 127.0.0.1 12345 "This command will run client connected 
		to server that is running in same machine in default port" 





